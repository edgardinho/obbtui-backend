'use strict';
var express = require('express'),
  estudianteService = require('../services/estudiante.service'),
  router = express.Router();

router.post('/periodo', estudianteService.getPeriodo);

module.exports = router;